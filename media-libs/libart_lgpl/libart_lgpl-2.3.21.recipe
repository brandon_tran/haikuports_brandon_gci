SUMMARY="Libart is a library for high-performance 2D graphics"
DESCRIPTION="Libart is free software (all components are either GPL or LGPL). Libart is also available for commercial licensing. Libart supports a very powerful imaging model, basically the same as SVG and the Java 2D API. It includes all PostScript imaging operations, and adds antialiasing and alpha-transparency.  Libart is also highly tuned for incremental rendering. It contains data structures and algorithms suited to rapid, precise computation of Region of Interest, as well as a two-phase rendering pipeline optimized for interactive display."
HOMEPAGE="http://www.levien.com/libart/"
SRC_URI="http://ftp.gnome.org/pub/gnome/sources/libart_lgpl/2.3/libart_lgpl-2.3.21.tar.gz"
CHECKSUM_MD5="722471ec8ae084af4293126d06b60880"
REVISION="1"
LICENSE="GNU LGPL v2.1"
COPYRIGHT="2001-2010 Mathieu Lacage and Raph Levien"

ARCHITECTURES="x86 x86_gcc2"

PROVIDES="
	libart_lgpl= $portVersion
	cmd:libart2_config = $portVersion compat >= 2
	lib:libart_lgpl_2 = 2.3.21 compat >= 2
	" 
REQUIRES="
	haiku >= $haikuVersion
	"
BUILD_REQUIRES="
	"
BUILD_PREREQUIRES="
	haiku_devel >= $haikuVersion
	cmd:aclocal
	cmd:autoreconf
	cmd:libtool
	cmd:make
	cmd:ld${secondaryArchSuffix}
	cmd:gcc${secondaryArchSuffix}
	"
BUILD()
{
	libtoolize --force --copy --install
	autoreconf -i
	runConfigure ./configure
	make $jobArgs
}
	
INSTALL()
{
	make install
	
	# prepare devel/lib
	prepareInstalledDevelLibs libart_lgpl_2
	
	# devel package
	packageEntries devel \
		$developDir
}

# ----- devel package

PROVIDES_devel="
	libart_lgpl_2 = $portVersion
	devel:libart_lgpl_2 = 2.3.21 compat >= 2
	"
REQUIRES_devel="
	libart_lgpl_2 == $portVersion base
	"
